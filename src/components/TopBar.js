import React, { Component } from 'react';

class TopBar extends Component {

    render(){
        return(
            <div>
                <div className="topbar">
                    <h1 className="top-bar-title">Hello World</h1>
                </div>
            </div>
        );
    }
}

export default TopBar;