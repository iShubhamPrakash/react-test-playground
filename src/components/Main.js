import React , { Component } from 'react';
import GridPage from './GridPage';
import Footer from './Footer';
import About from './About';
import Contact from './Contact';
import Users from './Users';
import { BrowserRouter, Route } from "react-router-dom";


class Main extends Component {
    render() {
      return (
          <div className="main">
            <Route exact path="/" component={GridPage} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/users" component={Users} />
            <Footer/>
          </div>
      );
    }
  }
  
  export default Main;