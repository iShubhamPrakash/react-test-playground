import React, { Component } from "react";
import logo from '../logo.svg';


let Grid= () => {
    return (
        <div className="col col-12 col-md-6 col-lg-4">
            <div className="card box">
            <h1 className="text-center">Box</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit inventore autem adipisci mole.</p>
            <img src={logo} className="App-logo" alt="logo" />
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit inventore autem adipisci molestiae repellendus consectetur natus accusantium ratione, magni nihil tempore aperiam qui error architecto repudiandae eligendi et fuga eius.</p>
            </div>
        </div>
    );
}



class GridPage extends Component {
    render(){

        return(
            <div>
                <img src={logo} className="App-logo" alt="logo" />
                <h1 className="color-red bg-yellow">Hello this is a sample text</h1>
                <button className="btn btn-danger">a button</button>
                <div className="container">
                    <div className="row">
                        <Grid/>
                        <Grid/>
                        <Grid/>
                        <Grid/>
                        <Grid/>
                        <Grid/>
                    </div>
                </div>
            </div>
        );
    }
}

export default GridPage;