import React, { Component } from 'react';

class About extends Component {
    render(){

        return(
            <div>
                <h1>About</h1>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <h2>Thing1</h2>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <h2>Thing 2</h2>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <h2>Thing 3</h2>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
                <p className="para">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ea fugiat quasi vel nemo ratione illum temporibus, dolorem accusamus ipsa! Repudiandae omnis impedit dolor in accusantium excepturi beatae aliquid modi.</p>
            </div>
        );
    }
}

export default About;