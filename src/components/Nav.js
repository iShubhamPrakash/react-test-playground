import React, { Component } from 'react';
import { Link } from "react-router-dom";






class Nav extends Component {

    render(){
        return(
            <div className="nav-container">
                <div className="row">
                    <div className="col col-12">
                        <Link to="/"><h1>Home</h1></Link>
                        <Link to="/about"><h1>About</h1></Link>
                        <Link to="/users"><h1>User</h1></Link>
                        <Link to="/contact"><h1>Contact</h1></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default Nav;