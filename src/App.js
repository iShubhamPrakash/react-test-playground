import React, { Component } from 'react';
import Nav from './components/Nav';
import TopBar from './components/TopBar';
import Main from './components/Main';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TopBar/>
        <Nav/>
        <Main/>
      </div>
    );
  }
}

export default App;
